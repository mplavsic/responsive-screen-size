import 'package:flutter/widgets.dart';

import 'breakpoints.dart';
import 'utils/chain.dart';

/// A pragmatic responsive-layout dispatcher.
class ScreenSizeDispatcher extends StatelessWidget {
  final Widget? child;
  final TransitionBuilder? smartphone;
  final TransitionBuilder? tablet;
  final TransitionBuilder? smallTablet;
  final TransitionBuilder? largeTablet;
  final TransitionBuilder? desktop;
  final TransitionBuilder other;

  const ScreenSizeDispatcher({
    super.key,
    this.child,
    required this.other,
    this.desktop,
    this.largeTablet,
    this.smallTablet,
    this.tablet,
    this.smartphone,
  });

  @override
  Widget build(BuildContext context) {
    final width = MediaQuery.of(context).size.width;
    final TransitionBuilder? caseChain;
    if (breakPoints.isSmartphone(width)) {
      caseChain = tbChain([smartphone]);
    } else if (breakPoints.isTabletSmall(width)) {
      caseChain = tbChain([smallTablet, tablet]);
    } else if (breakPoints.isTabletLarge(width)) {
      caseChain = tbChain([largeTablet, tablet]);
    } else {
      caseChain = tbChain([desktop]);
    }
    return tbChain([caseChain, other])!.call(context, child);
  }
}
