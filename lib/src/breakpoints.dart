/// Default breakpoints.
AbstractBreakpoints breakPoints = Breakpoints(600, 900, 1200);

/// It is possible to extend this class to achieve special behaviour.
abstract class AbstractBreakpoints {
  final double smartphoneMaxWidth;
  final double tabletSmallMaxWidth;
  final double tabletLargeMaxWidth;

  AbstractBreakpoints(
    this.smartphoneMaxWidth,
    this.tabletSmallMaxWidth,
    this.tabletLargeMaxWidth,
  );
  bool lessThan(double width, double logicalPixels) => width <= logicalPixels;

  //! In case a platform has way more logical pixels than the other ones,
  //! add the chain of that platform to the [matchPhysicalPlatform] below.

  bool isSmartphone(double width);

  bool isTabletSmall(double width);

  bool isTabletLarge(double width);
}

/// A simple implementation of [AbstractBreakpoints].
class Breakpoints extends AbstractBreakpoints {
  Breakpoints(
    super.smartphoneMaxWidth,
    super.tabletSmallMaxWidth,
    super.tabletLargeMaxWidth,
  );

  //! In case a platform has way more logical pixels than the other ones,
  //! add the chain of that platform to the [matchPhysicalPlatform] below.

  @override
  bool isSmartphone(double width) => lessThan(width, smartphoneMaxWidth);

  @override
  bool isTabletSmall(double width) => lessThan(width, tabletSmallMaxWidth);

  @override
  bool isTabletLarge(double width) => lessThan(width, tabletLargeMaxWidth);
}

/// A simple implementation of [AbstractBreakpoints].
class ABreakpoints extends AbstractBreakpoints {
  ABreakpoints() : super(500, 800, 1300);

  //! In case a platform has way more logical pixels than the other ones,
  //! add the chain of that platform to the [matchPhysicalPlatform] below.

  @override
  bool isSmartphone(double width) => lessThan(width, smartphoneMaxWidth);

  @override
  bool isTabletSmall(double width) => lessThan(width, tabletSmallMaxWidth);

  @override
  bool isTabletLarge(double width) => lessThan(width, tabletLargeMaxWidth);
}
