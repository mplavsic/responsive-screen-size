import 'package:flutter/widgets.dart';

import 'breakpoints.dart';
import 'utils/chain.dart';

T matchScreenSize<T>(
  BuildContext context, {
  T Function()? mobile,
  T Function()? smartphone,
  T Function()? tablet,
  T Function()? smallTablet,
  T Function()? largeTablet,
  T Function()? desktop,
  required T Function() other,
}) {
  final width = MediaQuery.of(context).size.width;
  final T Function()? caseChain;
  if (breakPoints.isSmartphone(width)) {
    caseChain = chain([smartphone, mobile]);
  } else if (breakPoints.isTabletSmall(width)) {
    caseChain = chain([smallTablet, tablet, mobile]);
  } else if (breakPoints.isTabletLarge(width)) {
    caseChain = chain([largeTablet, tablet, mobile]);
  } else {
    caseChain = chain([desktop]);
  }
  return chain([caseChain, other])!.call();
}
