import 'package:flutter/widgets.dart';

T Function()? chain<T>(List<T Function()?> chain) {
  for (final next in chain) {
    if (next != null) return next;
  }
  return null;
}

TransitionBuilder? tbChain<T>(List<TransitionBuilder?> chain) {
  for (final next in chain) {
    if (next != null) return next;
  }
  return null;
}
